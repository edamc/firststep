`timescale 1ns / 1ps
module Audio (clock, audio_reset_b, ac97_sdata_out, ac97_sdata_in,
		      ac97_synch, ac97_bit_clock);

   input clock;
   output audio_reset_b;
   output ac97_sdata_out;
   input ac97_sdata_in;
   output ac97_synch;
   input ac97_bit_clock;
	
   reg audio_reset_b;
   reg ac97_sdata_out;
   reg ac97_synch;
   
   reg [7:0] bit_count;
   reg [3:0] frame_count;
   
   reg [23:0] command;
   wire [19:0] command_data;
   wire [19:0] command_address;
	
	reg [17:0] DAC_data = 17'h000000;
	
   reg [7:0] reset_count;

   initial begin
      reset_count = 0;
      // synthesis attribute init of reset_count is "00";
      audio_reset_b = 1'b0;
      // synthesis attribute init of audio_reset_b is "0";
   end
   
   always @(posedge clock)
     if (reset_count == 255)
       audio_reset_b <= 1;
     else
       reset_count = reset_count+1;
   
   initial begin
      bit_count = 8'h00;
      // synthesis attribute init of bit_count is "00";
      frame_count = 4'h0;
      // synthesis attribute init of frame_count is "0";
   end
   
   always @(posedge ac97_bit_clock) begin
      // Generate the sync signal
      if (bit_count == 255)
	ac97_synch <= 1'b1;
      if (bit_count == 15)
	ac97_synch <= 1'b0;

      if ((bit_count >= 0) && (bit_count <= 15))
	// Slot 0: Tags
	case (bit_count[3:0])
	  4'h0: ac97_sdata_out <= 1; // Frame valid
	  4'h1: ac97_sdata_out <= 1; // Command address valid
	  4'h2: ac97_sdata_out <= 1; // Command data valid
	  4'h3: ac97_sdata_out <= 1;
	  4'h4: ac97_sdata_out <= 1;
	  default: ac97_sdata_out <= 1'b0;
	endcase
      
      else if ((bit_count >= 16) && (bit_count <= 35))
	// Slot 1: Command address
	ac97_sdata_out <= command_address[35-bit_count];
      
      else if ((bit_count >= 36) && (bit_count <= 55))
	// Slot 2: Command data
	ac97_sdata_out <= command_data[55-bit_count];
	
      else if ((bit_count >= 56) && (bit_count < 71))
	// Slot 3: PCM Left
		ac97_sdata_out <= DAC_data[73-bit_count];
   	
      else 
	ac97_sdata_out <= 1'b0;

      if (bit_count == 255) begin
			//if(frame_count == 255)
			//	frame_count <= 3;
			//else	
				frame_count <= frame_count+1;
		end
		
      bit_count <= bit_count+1;      
   end

   always @(frame_count)
     case (frame_count)
       4'h0: command = 24'h02_0000; // Unmute line outputs
       4'h1: command = 24'h04_0000; // Unmute headphones
       //4'h2: command = 24'h10_0808; // Unmute line inputs
		 4'h2: command = 24'h18_0808; // Unmute PCM ouputs
       default: command = 24'hFC_0000; // Read vendor ID
     endcase

	
	 //reg [3:0] sin_cnt = 0;	
	 always @(frame_count) begin
	 case (frame_count)
	  4'h0: DAC_data <= {18'h1FFFF} - {18'h1FFFF}; // 0
	  4'h1: DAC_data <= {18'h2C31E} - {18'h1FFFF}; // 0.3811
	  4'h2: DAC_data <= {18'h36A07} - {18'h1FFFF}; // 0.7071
	  4'h3: DAC_data <= {18'h3D907} - {18'h1FFFF}; // 0.9239
	  4'h4: DAC_data <= {18'h3FFFE} - {18'h1FFFF}; // 1
	  4'h5: DAC_data <= {18'h3D907} - {18'h1FFFF}; // 0.9239
	  4'h6: DAC_data <= {18'h36A07} - {18'h1FFFF}; // 0.7071
	  4'h7: DAC_data <= {18'h2C31E} - {18'h1FFFF}; // 0.3811
     4'h8: DAC_data <= {18'h1FFFF} - {18'h1FFFF}; // 0
	  4'h9: DAC_data <= {18'h13CE0} - {18'h1FFFF}; // -0.3811
	  4'hA: DAC_data <= {18'h095F7} - {18'h1FFFF}; // -0.7071
	  4'hB: DAC_data <= {18'h026F7} - {18'h1FFFF}; // -0.9239
     4'hC: DAC_data <= {18'h00001} - {18'h1FFFF}; // -1
	  4'hD: DAC_data <= {18'h026F7} - {18'h1FFFF}; // -0.9239
	  4'hE: DAC_data <= {18'h095F7} - {18'h1FFFF}; // -0.7071
	  4'hF: DAC_data <= {18'h13CE0} - {18'h1FFFF}; // -0.3811
	endcase
	//sin_cnt <= sin_cnt + 1;
	 //DAC_data <= DAC_data + sw; // -0.3811	  
	 end
    
   // Separate the address and data portions of the command
   // and pad them to 20 bits
   assign command_address = {command[23:16], 12'h000};
   assign command_data = {command[15:0], 4'h0};
   
endmodule

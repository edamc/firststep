`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:40:33 12/26/2017
// Design Name:   Audio
// Module Name:   /home/xilinx/Desktop/VerilogAudioLab/Kursach/Audio/Audio_testing.v
// Project Name:  Audio
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Audio
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Audio_testing;

	// Inputs
	reg clock;
	reg ac97_sdata_in;
	reg ac97_bit_clock;

	// Outputs
	wire audio_reset_b;
	wire ac97_sdata_out;
	wire ac97_synch;

	// Instantiate the Unit Under Test (UUT)
	Audio uut (
		.clock(clock), 
		.audio_reset_b(audio_reset_b), 
		.ac97_sdata_out(ac97_sdata_out), 
		.ac97_sdata_in(ac97_sdata_in), 
		.ac97_synch(ac97_synch), 
		.ac97_bit_clock(ac97_bit_clock)
	);

	initial begin
		// Initialize Inputs
		clock = 0;
		ac97_sdata_in = 0;
		ac97_bit_clock = 0;

		// Wait 100 ns for global reset to finish
		#100;
      end
		
		always begin #83 ac97_bit_clock = ~ac97_bit_clock;
		end
		always begin #2 clock = ~clock;
		end
		// Add stimulus here

	
      
endmodule

